#ifndef FPIMAGE_H
#define FPIMAGE_H


#include <QMainWindow>


namespace Ui {
class FPImage;
}

class FPImage : public QMainWindow
{
    Q_OBJECT
    
public:

    explicit FPImage(QWidget *parent = 0);
    ~FPImage();
    
private:

    Ui::FPImage *ui;

    QString Path;               // Para recordar la carpeta al cargar imágenes
    QString Name, BaseName;     // Nombres de los archivos a manejar
    QImage LowImage;
    QImage HiImage;

    int Wl, Hl;                 // Ancho y alto de una imagen LowRes
    int Sl;                     // "Step" (bytes por línea) de una imagen LowRes

    void ShowIt(void);          // Muestra la imagen actual
    //void gaussian(float matrix);

private slots:

    void Load(void);    // Slot para el botón de carga de imagen de baja resolución
    void DoIt(void);    // Slot para el botón de construir la imagen de alta resolución
    void on_pushButton_maximum_clicked();
    void on_pushButton_gaussian_clicked();
    void on_pushButton_clicked();
};

#endif // FPIMAGE_H
