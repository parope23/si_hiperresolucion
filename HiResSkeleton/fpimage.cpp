#include "fpimage.h"
#include "ui_fpimage.h"

#include <QFileDialog>
#include <QPainter>

#include <math.h>

#include <QLoggingCategory>


//-------------------------------------------------
//-- Constructor: Conexiones e inicializaciones ---
//-------------------------------------------------
FPImage::FPImage(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FPImage)
{

    ui->setupUi(this);

    // CONEXIONES de nuestros objetos (botones, etc...) a nuestros slots
    connect(ui->BSelectFile,SIGNAL(clicked()),this,SLOT(Load()));
    connect(ui->BRestore,SIGNAL(clicked()),this,SLOT(DoIt()));

    // INICIALIZACIONES
    Wl=Hl=0;      // Empezamos sin imagen cargada
    Path="..";  // Carpeta inicial
    Name.clear();

}

//-------------------------------------------------
//------ Destructor: Limpieza antes de salir ------
//-------------------------------------------------
FPImage::~FPImage()
{

    delete ui;

}

//-------------------------------------------------
//--------- Load and show a low-res image ---------
//-------------------------------------------------
void FPImage::Load(void)
{
    QString file=QFileDialog::getOpenFileName(this,tr("Open image"),Path,tr("BMP Image Files (*.bmp)"));
    if(file.isEmpty()) return;

    QFileInfo finfo(file);
    //Path="/Users/pablo/Documents/Repo/SI/Qt_hiperresolucion/Base (2)";
    //BaseName="Base";
    Path=finfo.path();
    BaseName=finfo.baseName();
    BaseName=BaseName.left(BaseName.indexOf('_'));

    ui->EFile->setText(BaseName);
    setWindowTitle("Super resolutive v0.1b - "+BaseName);

    LowImage.load(Path+"/"+BaseName+"_"+QString::number(0)+"_"+QString::number(0)+".bmp");

    Wl=LowImage.width();
    Hl=LowImage.height();
    Sl=LowImage.bytesPerLine();

    QImage AuxIma=LowImage.scaled(5*Wl,5*Hl);
    ui->Ecran->setPixmap(QPixmap::fromImage(AuxIma));

    AuxIma.load(Path+"/Suspect.jpg");
    ui->EcranSuspect->setPixmap(QPixmap::fromImage(AuxIma));

}

//-------------------------------------------------
//-------------- Build high-res image -------------
//-------------------------------------------------
void FPImage::DoIt(void)
{
    if(!Hl) return;

    // Nos aseguramos de que hay una imagen cargada
    if(BaseName.isEmpty()) return;
    ui->ERes->appendPlainText("Building "+BaseName);

    QLoggingCategory debug("debug");

    int W = 5*Wl+4;
    int H = 5*Hl+4;

    HiImage = QImage(W,H,QImage::Format_RGB888);

    int s = HiImage.bytesPerLine();
    qCDebug(debug) << s;
    uchar *pixR = HiImage.bits();
    uchar *pixG = pixR+1;
    uchar *pixB = pixG+1;

    int *auxR = new int[W*H*3];
    int *auxG = auxR + 1;
    int *auxB = auxG + 1;
    memset(auxR,0,W*H*3*sizeof(int));

    for(int n_imagen = 0; n_imagen<25; n_imagen++){
        int desp_columna = n_imagen%5;
        int desp_fila = n_imagen/5;

        qCDebug(debug) << "n_imagen";
        qCDebug(debug) << n_imagen;
        qCDebug(debug) << "horizontal";
        qCDebug(debug) << desp_columna;
        qCDebug(debug) << "vertical";
        qCDebug(debug) << desp_fila;

        QImage TempImage;
        TempImage.load(Path+"/"+BaseName+"_"+QString::number(desp_columna)+"_"+QString::number(desp_fila)+".bmp");
        TempImage = TempImage.convertToFormat(QImage::Format_RGB888);
        uchar *pixR_t = TempImage.bits();
        uchar *pixG_t = pixR_t+1;
        uchar *pixB_t = pixG_t+1;
        int s_t = TempImage.bytesPerLine();

        for(int fila = 0; fila<Hl; fila++){
            for(int columna = 0; columna<Wl; columna++){
                for(int i = 0; i < 5; i++){ // El bucle sirve para recorrer las 5 filas de la imagen final a los que afecta cada pixel original
                    for(int j = 0; j < 5; j++){ // El bucle sirve para recorrer las 5 columnas de la imagen final a los que afecta cada pixel original

                        // Red
                        auxR[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] += pixR_t[s_t*fila + 3*columna];

                        // Green
                        auxG[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] += pixG_t[s_t*fila + 3*columna];

                        // Blue
                        auxB[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] += pixB_t[s_t*fila + 3*columna];
                    }
                }
            }
        }
    }

    for(int fila = 0; fila<5*Hl+4; fila++){
        for(int columna = 0; columna<5*Wl+4; columna++){
            pixR[s*fila+3*columna] = auxR[s*fila+3*columna]/25;
            pixG[s*fila+3*columna] = auxG[s*fila+3*columna]/25;
            pixB[s*fila+3*columna] = auxB[s*fila+3*columna]/25;
        }
    }

    // Sacamos algo de texto informativo
    ui->ERes->appendPlainText("Superresolved using average value");
    statusBar()->showMessage("Superresolved using average value");

    // Mostramos HiImage en pantalla
    ShowIt();

    delete [] auxR;
}

//-------------------------------------------------
//-------------- Mostramos la imagen --------------
//-------------------------------------------------
inline void FPImage::ShowIt(void)
{
    // Creamos un lienzo (QPixmap) a partir de la QImage
    // y lo asignamos a la QLabel de la derecha
    ui->EcranBis->setPixmap(QPixmap::fromImage(HiImage));
}

// ---------------- Maximum -----------------------
void FPImage::on_pushButton_maximum_clicked()
{
    if(!Hl) return;

    // Nos aseguramos de que hay una imagen cargada
    if(BaseName.isEmpty()) return;
    ui->ERes->appendPlainText("Building "+BaseName);

    QLoggingCategory debug("debug");

    int W = 5*Wl+4;
    int H = 5*Hl+4;

    HiImage = QImage(W,H,QImage::Format_RGB888);

    int s = HiImage.bytesPerLine();
    qCDebug(debug) << s;
    uchar *pixR = HiImage.bits();
    uchar *pixG = pixR+1;
    uchar *pixB = pixG+1;

    int *auxR = new int[W*H*3];
    int *auxG = auxR + 1;
    int *auxB = auxG + 1;
    memset(auxR,0,W*H*3*sizeof(int));

    for(int n_imagen = 0; n_imagen<25; n_imagen++){
        int desp_columna = n_imagen%5;
        int desp_fila = n_imagen/5;

        qCDebug(debug) << "n_imagen";
        qCDebug(debug) << n_imagen;
        qCDebug(debug) << "horizontal";
        qCDebug(debug) << desp_columna;
        qCDebug(debug) << "vertical";
        qCDebug(debug) << desp_fila;

        QImage TempImage;
        TempImage.load(Path+"/"+BaseName+"_"+QString::number(desp_columna)+"_"+QString::number(desp_fila)+".bmp");
        TempImage = TempImage.convertToFormat(QImage::Format_RGB888);
        uchar *pixR_t = TempImage.bits();
        uchar *pixG_t = pixR_t+1;
        uchar *pixB_t = pixG_t+1;
        int s_t = TempImage.bytesPerLine();

        for(int fila = 0; fila<Hl; fila++){
            for(int columna = 0; columna<Wl; columna++){
                for(int i = 0; i < 5; i++){ // El bucle sirve para recorrer las 5 filas de la imagen final a los que afecta cada pixel original
                    for(int j = 0; j < 5; j++){ // El bucle sirve para recorrer las 5 columnas de la imagen final a los que afecta cada pixel original

                        // Red
                        if(auxR[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] < pixR_t[s_t*fila + 3*columna])
                        auxR[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] = pixR_t[s_t*fila + 3*columna];

                        // Green
                        if(auxG[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] < pixG_t[s_t*fila + 3*columna])
                        auxG[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] = pixG_t[s_t*fila + 3*columna];

                        // Blue
                        if(auxB[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] < pixB_t[s_t*fila + 3*columna])
                        auxB[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] = pixB_t[s_t*fila + 3*columna];
                    }
                }
            }
        }
    }

    for(int fila = 0; fila<5*Hl+4; fila++){
        for(int columna = 0; columna<5*Wl+4; columna++){
            pixR[s*fila+3*columna] = auxR[s*fila+3*columna];
            pixG[s*fila+3*columna] = auxG[s*fila+3*columna];
            pixB[s*fila+3*columna] = auxB[s*fila+3*columna];
        }
    }

    // Sacamos algo de texto informativo
    ui->ERes->appendPlainText("Superresolved using maximum value");
    statusBar()->showMessage("Superresolved using maximum value");

    // Mostramos HiImage en pantalla
    ShowIt();

    delete [] auxR;
}

//----------------- Gaussian ----------------------
void FPImage::on_pushButton_clicked()
{
    if(!Hl) return;

    // Nos aseguramos de que hay una imagen cargada
    if(BaseName.isEmpty()) return;
    ui->ERes->appendPlainText("Building "+BaseName);

    QLoggingCategory debug("debug");

    int W = 5*Wl+4;
    int H = 5*Hl+4;

    HiImage = QImage(W,H,QImage::Format_RGB888);

    int s = HiImage.bytesPerLine();
    qCDebug(debug) << s;
    uchar *pixR = HiImage.bits();
    uchar *pixG = pixR+1;
    uchar *pixB = pixG+1;

    int *auxR = new int[W*H*3];
    int *auxG = auxR + 1;
    int *auxB = auxG + 1;
    memset(auxR,0,W*H*3*sizeof(int));

    // Preparar matriz gaussiana
    float matriz_gaussiana[][5] = {{0,0,0,0,0},{0,0.0038,0.9745,0.0038,0},{0,0.9745,252.0867,0.9745,0},{0,0.0038,0.9745,0.0038,0},{0,0,0,0,0}};

    for (int a = 0; a < 5; ++a) {
      for (int b = 0; b < 5; ++b) {
        matriz_gaussiana[a][b] = matriz_gaussiana[a][b]/256;
      }
    }

    for(int n_imagen = 0; n_imagen<25; n_imagen++){
        int desp_columna = n_imagen%5;
        int desp_fila = n_imagen/5;

        qCDebug(debug) << "n_imagen";
        qCDebug(debug) << n_imagen;
        qCDebug(debug) << "horizontal";
        qCDebug(debug) << desp_columna;
        qCDebug(debug) << "vertical";
        qCDebug(debug) << desp_fila;

        QImage TempImage;
        TempImage.load(Path+"/"+BaseName+"_"+QString::number(desp_columna)+"_"+QString::number(desp_fila)+".bmp");
        TempImage = TempImage.convertToFormat(QImage::Format_RGB888);
        uchar *pixR_t = TempImage.bits();
        uchar *pixG_t = pixR_t+1;
        uchar *pixB_t = pixG_t+1;
        int s_t = TempImage.bytesPerLine();

        for(int fila = 0; fila<Hl; fila++){
            for(int columna = 0; columna<Wl; columna++){
                for(int i = 0; i < 5; i++){ // El bucle sirve para recorrer las 5 filas de la imagen final a los que afecta cada pixel original
                    for(int j = 0; j < 5; j++){ // El bucle sirve para recorrer las 5 columnas de la imagen final a los que afecta cada pixel original

                        // Red
                        auxR[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] += pixR_t[s_t*fila + 3*columna]*matriz_gaussiana[i][j];

                        // Green
                        auxG[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] += pixG_t[s_t*fila + 3*columna]*matriz_gaussiana[i][j];

                        // Blue
                        auxB[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] += pixB_t[s_t*fila + 3*columna]*matriz_gaussiana[i][j];
                    }
                }
            }
        }
    }

    for(int fila = 0; fila<5*Hl+4; fila++){
        for(int columna = 0; columna<5*Wl+4; columna++){
            pixR[s*fila+3*columna] = auxR[s*fila+3*columna];
            pixG[s*fila+3*columna] = auxG[s*fila+3*columna];
            pixB[s*fila+3*columna] = auxB[s*fila+3*columna];
        }
    }

    // Sacamos algo de texto informativo
    ui->ERes->appendPlainText("Superresolved using gaussian with sigma = 0.3");
    statusBar()->showMessage("Superresolved using gaussian with sigma = 0.3");

    // Mostramos HiImage en pantalla
    ShowIt();

    delete [] auxR;
}

void FPImage::on_pushButton_gaussian_clicked()
{
    if(!Hl) return;

    // Nos aseguramos de que hay una imagen cargada
    if(BaseName.isEmpty()) return;
    ui->ERes->appendPlainText("Building "+BaseName);

    QLoggingCategory debug("debug");

    int W = 5*Wl+4;
    int H = 5*Hl+4;

    HiImage = QImage(W,H,QImage::Format_RGB888);

    int s = HiImage.bytesPerLine();
    qCDebug(debug) << s;
    uchar *pixR = HiImage.bits();
    uchar *pixG = pixR+1;
    uchar *pixB = pixG+1;

    int *auxR = new int[W*H*3];
    int *auxG = auxR + 1;
    int *auxB = auxG + 1;
    memset(auxR,0,W*H*3*sizeof(int));

    // Preparar matriz gaussiana
    float matriz_gaussiana[][5] = {{0.7601,3.4064,5.6162,3.4064,0.7601},{3.4064,15.2664,25.1700,15.2664,3.4064},{5.6162,25.1700,41.4983,25.1700,5.6162},{3.4064,15.2664,25.1700,15.2664,3.4064},{0.7601,3.4064,5.6162,3.4064,0.7601}};
    for (int a = 0; a < 5; ++a) {
      for (int b = 0; b < 5; ++b) {
        matriz_gaussiana[a][b] = matriz_gaussiana[a][b]/256;
      }
    }

    for(int n_imagen = 0; n_imagen<25; n_imagen++){
        int desp_columna = n_imagen%5;
        int desp_fila = n_imagen/5;

        qCDebug(debug) << "n_imagen";
        qCDebug(debug) << n_imagen;
        qCDebug(debug) << "horizontal";
        qCDebug(debug) << desp_columna;
        qCDebug(debug) << "vertical";
        qCDebug(debug) << desp_fila;

        QImage TempImage;
        TempImage.load(Path+"/"+BaseName+"_"+QString::number(desp_columna)+"_"+QString::number(desp_fila)+".bmp");
        TempImage = TempImage.convertToFormat(QImage::Format_RGB888);
        uchar *pixR_t = TempImage.bits();
        uchar *pixG_t = pixR_t+1;
        uchar *pixB_t = pixG_t+1;
        int s_t = TempImage.bytesPerLine();

        for(int fila = 0; fila<Hl; fila++){
            for(int columna = 0; columna<Wl; columna++){
                for(int i = 0; i < 5; i++){ // El bucle sirve para recorrer las 5 filas de la imagen final a los que afecta cada pixel original
                    for(int j = 0; j < 5; j++){ // El bucle sirve para recorrer las 5 columnas de la imagen final a los que afecta cada pixel original

                        // Red
                        auxR[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] += pixR_t[s_t*fila + 3*columna]*matriz_gaussiana[i][j];

                        // Green
                        auxG[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] += pixG_t[s_t*fila + 3*columna]*matriz_gaussiana[i][j];

                        // Blue
                        auxB[s*5*fila + s*(desp_fila+i) + 5*3*columna + 3*(desp_columna+j)] += pixB_t[s_t*fila + 3*columna]*matriz_gaussiana[i][j];
                    }
                }
            }
        }
    }

    for(int fila = 0; fila<5*Hl+4; fila++){
        for(int columna = 0; columna<5*Wl+4; columna++){
            pixR[s*fila+3*columna] = auxR[s*fila+3*columna];
            pixG[s*fila+3*columna] = auxG[s*fila+3*columna];
            pixB[s*fila+3*columna] = auxB[s*fila+3*columna];
        }
    }

    // Sacamos algo de texto informativo
    ui->ERes->appendPlainText("Superresolved using gaussian with sigma = 1.0");
    statusBar()->showMessage("Superresolved using gaussian with sigma = 1.0");

    // Mostramos HiImage en pantalla
    ShowIt();

    delete [] auxR;
}
